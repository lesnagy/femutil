#
# Library configuration file used by dependent projects.
# @file:   femutil-config.cmake
# @author: L. Nagy
#

if (NOT DEFINED femutil_FOUND)

  # Locate the library headers.
  find_path(femutil_include_dir
    NAMES tetra_mesh_utils.h
    PATHS ${femutil_DIR}/src
  )

  # Export libary targets.
  set(femutil_libraries
    femutil
    CACHE INTERNAL "femutil library" FORCE
  )

  # Handle REQUIRED, QUIED and version related arguments to find_package(...)
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(
    femutil DEFAULT_MSG
    femutil_include_dir
    femutil_libraries
  )

  # If the project is not `femutil` then make directories. 
  if (NOT ${PROJECT_NAME} STREQUAL femutil)
    add_subdirectory(
      ${femutil_DIR}
      ${CMAKE_CURRENT_BINARY_DIR}/femutil
    )
  endif()

endif()
