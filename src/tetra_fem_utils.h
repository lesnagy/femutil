/**
 * @file   tetra_fem_utils.h
 * @author L. Nagy
 */

#ifndef TETRA_FEM_UTILS_H
#define TETRA_FEM_UTILS_H

#include <boost/multi_array.hpp>

void box_volumes(boost::multi_array<double, 2> const &vertices,
                 boost::multi_array<int, 2>    const &elements,
                 boost::multi_array<double, 1>          &boxvol);

#endif
