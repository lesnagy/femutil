/**
 * @file   tetra_fem_utils.cxx
 * @author L. Nagy
 */

#include "tetra_fem_utils.h"

using namespace boost;

void box_volumes(multi_array<double, 2> const &vertices,
                 multi_array<int, 2>    const &elements,
                 multi_array<double, 1>       &boxvol)
{
}
