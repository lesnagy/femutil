
#include <iostream>

#include "tetra_fem_utils.h"

using namespace std;
using namespace boost;

void test_boxvol(bool &test_status);

int main(int argc, char **argv)
{
  bool all_status = true;
  bool status = true;

  /////////////////////////////////////////////////////////////////////////////
  // TEST: test_boxvol().                                                    //
  /////////////////////////////////////////////////////////////////////////////
  status = true;
  test_boxvol(status);
  if (status == false) {
    cout << "FAILED test_boxvol()" << endl;
    all_status = false;
  } else {
    cout <<"OK      test_boxvol()" << endl;
  }

  if (all_status == false) {
    return 1;
  }
  return 0;
}

void test_boxvol(bool &test_status) 
{
  test_status = false;
}
